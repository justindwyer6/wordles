#!/usr/bin/env python

import dominate
from dominate.tags import *
from dominate.util import raw
import yaml

languages = list(yaml.load_all(open('languages.yaml'), Loader=yaml.SafeLoader))[0]
wordles = list(yaml.load_all(open('wordles.yaml'), Loader=yaml.SafeLoader))[0]

langs = {}

for language in languages:
    langs[language['iso']] = {
        'name': language['name'],
        'name-en': language['name-en'],
        'wiki': language['wiki'],
        'wiki-lang': language['wiki-lang'],
        'wiki-en': language['wiki-en'],
        }
    if 'css' in language:
        langs[language['iso']]['css'] = language['css']

doc = dominate.document(title='Wordles of the World')

with doc.head:
    #  link(rel='stylesheet', href='mini-default.min.css')
    #  link(rel='stylesheet', href='pure-min.css')
    link(rel='stylesheet', href="https://unpkg.com/purecss@2.0.6/build/pure-min.css", integrity="sha384-Uu6IeWbM+gzNVXJcM9XV3SohHtmWE+3VGi496jvgX1jyvDTXfdK+rfZc8C1Aehk5", crossorigin="anonymous")
    meta(name="viewport", content="width=device-width, initial-scale=1")
    link(rel='stylesheet', href='style.css')
    raw('''
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Gentium+Basic:ital,wght@0,400;0,700;1,400;1,700&display=swap" rel="stylesheet"> 
            ''')
    script(src="https://twemoji.maxcdn.com/v/latest/twemoji.min.js", crossorigin="anonymous")
    link(rel='icon', href='logo.png')

with doc.body:
    with div(cls='content-wrapper'):
        with div(cls='content'):
            h1("🟩 Wordles of the world, unite!")
            with p(__pretty=False):
                span("This is — to the best of my knowledge — the most comprehensive list of ")
                a("Wordle", href="https://www.powerlanguage.co.uk/wordle/")
                span("-like games online. It currently contains ")
                span(str(len(wordles)+1))
                span(" entries in ")
                span(str(len(languages)))
                span(" languages. Do you know of any clone or version that is absent from the list? Please ")
                a("contact me", href="http://me.digitalwords.net/")
                span(" or make a ")
                with a(href="https://gitlab.com/rwmpelstilzchen/wordles"):
                    span("pull request ")
                    img(src='gitlab.svg', cls='icon')
                span(" and I will be happy to add it 🙂")
            with p(__pretty=False):
                span("This list is based on lists by ")
                a("Kurt", href="https://github.com/thiskurt/wordle-languages")
                span(" and ")
                a("Sam", href="https://gist.github.com/settinger/7dbadc5646b8f58e4f29562305af03fe")
                span(", and includes versions I came upon on ")
                a("Twitter", href="https://twitter.com/JudaRonen/status/1484274702273105921")
                span(" and elsewhere as well.")
            with table(cls='pure-table'):
                with thead():
                    with tr():
                        th("Language", colspan='2')
                        th("Name")
                        th("Code")
                        th("Note")
                prevlang = ''
                with tbody():
                    for wordle in wordles:
                        with tr():
                            if prevlang != wordle['lang']:
                                prevlang = wordle['lang']
                                td(a(
                                    langs[wordle['lang']]['name-en'],
                                    href="https://en.wikipedia.org/wiki/" + langs[wordle['lang']]['wiki-en'],
                                    cls="lowkeylink"
                                    ))
                                if 'css' in langs[wordle['lang']]:
                                    td(a(
                                        langs[wordle['lang']]['name'],
                                        href="https://" + langs[wordle['lang']]['wiki-lang'] + ".wikipedia.org/wiki/" + langs[wordle['lang']]['wiki'],
                                        cls="lowkeylink"
                                        ), cls=langs[wordle['lang']]['css'])
                                else:
                                    td(a(
                                        langs[wordle['lang']]['name'],
                                        href="https://" + langs[wordle['lang']]['wiki-lang'] + ".wikipedia.org/wiki/" + langs[wordle['lang']]['wiki'],
                                        cls="lowkeylink"
                                        ))
                            else:
                                td()
                                td()
                            if 'css' in wordle:
                                td(a(wordle['name'], href=wordle['url']), cls=wordle['css'])
                            else:
                                td(a(wordle['name'], href=wordle['url']))
                            if 'src' in wordle:
                                if wordle['src-type'] == 'github':
                                    td(a(img(src='github.svg', cls='icon'), href=wordle['src']))
                                elif wordle['src-type'] == 'git':
                                    td(a(img(src='git.svg', cls='icon'), href=wordle['src']))
                                elif wordle['src-type'] == 'other':
                                    td(a(img(src='code.svg', cls='icon'), href=wordle['src']))
                            else:
                                td()
                            if 'note' in wordle:
                                td(wordle['note'])
                            else:
                                td()

            with p(__pretty=False):
                span("In addition, there is at least one non-linguistic ‘Wordle’: ")
                a("Primel", href="https://converged.yt/primel/")
                span(" ")
                a(img(src="github.svg", cls="icon"), href="https://github.com/dill/primel")
                span(" (prime numbers).")
            with div(style="margin-top: 3em; text-align: center"):
                img(src="wordle.svg", style="width: 80%")
                with p(__pretty=False):
                    span("A ")
                    a("word cloud", href="https://en.wikipedia.org/wiki/Tag_cloud")
                    span(" (a.k.a. ")
                    em("wordle")
                    span(") made with ")
                    a("Jason Davies", href="https://www.jasondavies.com/")
                    span("’s open source")
                    a(" generator", href="https://www.jasondavies.com/wordcloud/")
                    span(".")
    script("twemoji.parse(document.body);")

print(doc)
